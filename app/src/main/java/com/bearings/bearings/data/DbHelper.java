package com.bearings.bearings.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bearings.bearings.data.BearingContract.*;


public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "bearings.db";
    public static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_TABLE_MAIN = "CREATE TABLE " + BearingEntry.TABLE_NAME_MAIN + " ("
                + BearingEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + BearingEntry.COLUMN_CODE + " TEXT NOT NULL, "
                + BearingEntry.COLUMN_NAME + " TEXT NOT NULL, "
                + BearingEntry.COLUMN_MANUFACTURER + " TEXT, "
                + BearingEntry.COLUMN_QUANTITY + " INTEGER);";
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_MAIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST " + BearingEntry.TABLE_NAME_MAIN);
    }
}
