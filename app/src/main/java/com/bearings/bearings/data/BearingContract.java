package com.bearings.bearings.data;

import android.provider.BaseColumns;


public class BearingContract {
    public static final class BearingEntry implements BaseColumns {
        public static final String TABLE_NAME_MAIN = "maintable";
        public static final String TABLE_NAME_PLACE = "placetable";
        public static final String COLUMN_CODE = "code";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_MANUFACTURER = "manufacturer";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_HANGAR = "hangar";
        public static final String COLUMN_RACK = "rack";
        public static final String COLUMN_PLACE = "place";
        public static final String COLUMN_QUANTITY_ON_PLACE = "quantity on place";
    }
}
