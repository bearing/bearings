package com.bearings.bearings.activities;


import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bearings.bearings.R;
import com.bearings.bearings.data.BearingContract;
import com.bearings.bearings.data.DbHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DownloadFileActivity extends AppCompatActivity {
    private Button btnChooseFilePath;
    private TextView tvFilePath;
    ProgressBar pbDownloadProgress;
    private int FILE_SELECT_CODE = 22;
    private boolean isFileChoosed = false;
    private SQLiteDatabase mDb;
    private final String LOG_TAG = "myLog";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_file);
        findViewsAndListeners();
        DbHelper dbHelper = new DbHelper(this);
        mDb = dbHelper.getWritableDatabase();

    }

    private void findViewsAndListeners() {
        btnChooseFilePath = (Button) findViewById(R.id.btn_choose_file);
        tvFilePath = (TextView) findViewById(R.id.tv_file_path);
        pbDownloadProgress = (ProgressBar) findViewById(R.id.pb_down_file);
        pbDownloadProgress.setVisibility(View.INVISIBLE);
        btnChooseFilePath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFileChoosed) {
                    pickFile();
                }
                if (isFileChoosed) {
                    FileDownloadTask fileDownloadTask = new FileDownloadTask();
                    fileDownloadTask.execute();

                }
            }
        });
    }

    // Метод позволяет выбрать файл из хранилища
    private void pickFile() {
        Intent pickFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pickFileIntent.setType("text/txt");
        pickFileIntent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(pickFileIntent, "Select a file to upload"), FILE_SELECT_CODE);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a file Manager.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_SELECT_CODE && resultCode == -1) {
            Uri fileUri = data.getData();
            String fileSelected = fileUri.getPath();
            tvFilePath.setText(fileSelected);
            btnChooseFilePath.setText(getString(R.string.download_file));
            isFileChoosed = true;
        }
    }

    private void readFile() {
//        File sdcard = Environment.getExternalStorageDirectory();
        //code=9; manufacturer=13; name=36; quantity=7
        if (isFileChoosed) {
            String code = null;
            String name = null;
            String manufacturer = null;
            int quantity = 0;

            //Get the text file
            File file = new File(tvFilePath.getText().toString());

            //Read text from file
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    // TODO: 04.03.2017 remove hardcode
                    code = line.substring(0, 8).trim();
                    manufacturer = line.substring(10, 22).trim();
                    name = line.substring(24, 59).trim();
                    String strQuantity = line.substring(61, 67).trim();
                    if (TextUtils.isEmpty(strQuantity)) {
                        quantity = 0;
                    } else {
                        quantity = Integer.parseInt(strQuantity);
                    }
                    ContentValues cv = new ContentValues();
                    cv.put(BearingContract.BearingEntry.COLUMN_CODE, code);
                    cv.put(BearingContract.BearingEntry.COLUMN_NAME, name);
                    cv.put(BearingContract.BearingEntry.COLUMN_MANUFACTURER, manufacturer);
                    cv.put(BearingContract.BearingEntry.COLUMN_QUANTITY, quantity);
                    mDb.insert(BearingContract.BearingEntry.TABLE_NAME_MAIN, null, cv);
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }

    }

    class FileDownloadTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbDownloadProgress.setVisibility(View.VISIBLE);
            btnChooseFilePath.setEnabled(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            readFile();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pbDownloadProgress.setVisibility(View.INVISIBLE);
            btnChooseFilePath.setEnabled(true);
        }
    }
}
