package com.bearings.bearings.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bearings.bearings.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnDownloadFile;
    private Button btnViewBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewsAndListeners();
    }

    //find all views and set listener
    private void findViewsAndListeners() {
        btnDownloadFile = (Button) findViewById(R.id.btn_download_file);
        btnViewBase = (Button) findViewById(R.id.btn_view_base);
        btnDownloadFile.setOnClickListener(this);
        btnViewBase.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_download_file:
                startActivity(new Intent(MainActivity.this, DownloadFileActivity.class));
                break;
            case R.id.btn_view_base:
                startActivity(new Intent(MainActivity.this, ViewBaseActivity.class));
                break;
        }
    }
}
