package com.bearings.bearings.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bearings.bearings.R;

public class ViewBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_base);
    }
}
